package com.devcamp.campainpizza365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Campainpizza365Application {

	public static void main(String[] args) {
		SpringApplication.run(Campainpizza365Application.class, args);
	}

}
