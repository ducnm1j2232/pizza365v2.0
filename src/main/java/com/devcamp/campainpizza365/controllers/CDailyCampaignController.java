package com.devcamp.campainpizza365.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.campainpizza365.models.Combo;
import com.devcamp.campainpizza365.services.CDailyCampaignService;

@RestController
@CrossOrigin
@RequestMapping("/pizza365")
public class CDailyCampaignController {
    @Autowired
    private CDailyCampaignService cDailyCampaignService;

    @GetMapping("/promotion")
    public String getPromotion() {
        String promotion = "";
        promotion = cDailyCampaignService.getTodayPromotion();
        return promotion;
    }

}
