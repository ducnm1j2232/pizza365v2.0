package com.devcamp.campainpizza365.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.campainpizza365.models.Drink;
import com.devcamp.campainpizza365.services.CDrinkService;

@RestController
@CrossOrigin
@RequestMapping("/pizza365")
public class CDrinkController {
    @Autowired
    private CDrinkService cDrinkService;

    @GetMapping("/drinks")
    public ArrayList<Drink> getDrinks() {
        return cDrinkService.getDrinks();
    }
}
