package com.devcamp.campainpizza365.converters;

public class DateTimeConverter {
    public static String convertDayOfWeekToVN(String dayOfWeek) {
        String vietnameseDayOfWeek;
        switch(dayOfWeek.toLowerCase()) {
            case "monday":
                vietnameseDayOfWeek = "Thứ Hai";
                break;
            case "tuesday":
                vietnameseDayOfWeek = "Thứ Ba";
                break;
            case "wednesday":
                vietnameseDayOfWeek = "Thứ Tư";
                break;
            case "thursday":
                vietnameseDayOfWeek = "Thứ Năm";
                break;
            case "friday":
                vietnameseDayOfWeek = "Thứ Sáu";
                break;
            case "saturday":
                vietnameseDayOfWeek = "Thứ Bảy";
                break;
            case "sunday":
                vietnameseDayOfWeek = "Chủ Nhật";
                break;
            default:
                vietnameseDayOfWeek = "Không xác định";
        }
        return vietnameseDayOfWeek;
    }
}
