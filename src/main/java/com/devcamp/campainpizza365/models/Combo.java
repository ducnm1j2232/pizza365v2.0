package com.devcamp.campainpizza365.models;

public class Combo {
    private String comboName;
    private String duongKinh;
    private int suongNuong;
    private String saladGr;
    private int soLuongNuocNgot;
    private double priceVND;
    private String fullName;

    public Combo(String comboName, String duongKinh, int suongNuong, String saladGr, int soLuongNuocNgot,
            double priceVND) {
        this.comboName = comboName;
        this.duongKinh = duongKinh;
        this.suongNuong = suongNuong;
        this.saladGr = saladGr;
        this.soLuongNuocNgot = soLuongNuocNgot;
        this.priceVND = priceVND;
    }

    public String getComboName() {
        return comboName;
    }

    public void setComboName(String comboName) {
        this.comboName = comboName;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuongNuong() {
        return suongNuong;
    }

    public void setSuongNuong(int suongNuong) {
        this.suongNuong = suongNuong;
    }

    public String getSaladGr() {
        return saladGr;
    }

    public void setSaladGr(String saladGr) {
        this.saladGr = saladGr;
    }

    public int getSoLuongNuocNgot() {
        return soLuongNuocNgot;
    }

    public void setSoLuongNuocNgot(int soLuongNuocNgot) {
        this.soLuongNuocNgot = soLuongNuocNgot;
    }

    public double getPriceVND() {
        return priceVND;
    }

    public void setPriceVND(double priceVND) {
        this.priceVND = priceVND;
    }

    public double priceAnnualUSD() {
        return this.priceVND * 12 * 0.9;
    }

    public String getFullName() {
        // String fullname = "";
        switch (this.comboName.toLowerCase()) {
            case "s":
                this.fullName = "S (Small)";
                break;
            case "m":
                this.fullName = "M (Medium)";
                break;
            case "l":
                this.fullName = "L (Large)";
                break;
        }

        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
