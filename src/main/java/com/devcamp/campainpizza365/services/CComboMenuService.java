package com.devcamp.campainpizza365.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.campainpizza365.models.Combo;

@Service
public class CComboMenuService {
    public ArrayList<Combo> getAllCombos() {
        ArrayList<Combo> combos = new ArrayList<Combo>();
        combos.add(getComboByName("S"));
        combos.add(getComboByName("M"));
        combos.add(getComboByName("L"));
        return combos;
    }

    public Combo getComboByName(String name) {
        Combo combo = null;
        switch (name.toLowerCase()) {
            case "s":
                combo = new Combo(name, "20 cm", 2, "200g", 2, 150000);
                break;
            case "m":
                combo = new Combo(name, "25 cm", 4, "300g", 3, 200000);
                break;
            case "l":
                combo = new Combo(name, "30 cm", 8, "500g", 4, 250000);
                break;
        }
        return combo;
    }
}
