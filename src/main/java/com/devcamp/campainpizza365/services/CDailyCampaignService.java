package com.devcamp.campainpizza365.services;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.campainpizza365.converters.DateTimeConverter;
import com.devcamp.campainpizza365.models.Combo;

@Service
public class CDailyCampaignService {
    public String getTodayPromotion() {
        String result = "";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        int dayOfWeekValue = dayOfWeek.getValue() + 1;
        result = DateTimeConverter.convertDayOfWeekToVN(dayOfWeek.name()) + ", mua " + dayOfWeekValue + " tặng "
                + dayOfWeekValue;
        return result;

    }

    
}
