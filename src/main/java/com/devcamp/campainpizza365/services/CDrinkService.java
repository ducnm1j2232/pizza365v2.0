package com.devcamp.campainpizza365.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.campainpizza365.models.Drink;

@Service
public class CDrinkService {
    public ArrayList<Drink> getDrinks() {
        ArrayList<Drink> drinks = new ArrayList<Drink>();
        drinks.add(new Drink("TRATAC", "Trà tắc", 10000, null, 1615177934000L, 1615177934000L));
        drinks.add(new Drink("COCA", "Cocacola", 15000, null, 1615177934000L, 1615177934000L));
        drinks.add(new Drink("PEPSI", "Pepsi", 15000, null, 1615177934000L, 1615177934000L));
        drinks.add(new Drink("LAVIE", "Lavie", 5000, null, 1615177934000L, 1615177934000L));
        drinks.add(new Drink("TRASUA", "Trà sữa trân châu", 40000, null, 1615177934000L, 1615177934000L));
        drinks.add(new Drink("FANTA", "Fanta", 15000, null, 1615177934000L, 1615177934000L));
        return drinks;
    }
}
